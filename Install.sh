#TRSS OneBot 安装脚本 作者：时雨🌌星空
NAME=v1.0.0;VERSION=202301020
R="[1;31m" G="[1;32m" Y="[1;33m" C="[1;36m" B="[1;m" O="[m"
echo "$B————————————————————————————
$R TRSS$Y OneBot$G Install$C Script$O
     $G$NAME$C ($VERSION)$O
$B————————————————————————————
      $G作者：$C时雨🌌星空$O"
abort(){ echo "
$R! $@$O";exit 1;}
DIR="${DIR:-$HOME/TRSS_OneBot}"
CMD="${CMD:-tsob}"
CMDPATH="${CMDPATH:-$PREFIX/bin}"
type pkg &>/dev/null&&echo "
$Y- 正在安装依赖$O
"||abort "找不到 pkg 命令，请确认安装了正确的 Termux 环境"
echo "extra-keys = [ ['ESC','<','>','BACKSLASH','=','^','$','()','{}','[]','ENTER'], ['TAB','&',';','/','~','%','*','HOME','UP','END','PGUP'], ['CTRL','FN','ALT','|','-','+','QUOTE','LEFT','DOWN','RIGHT','PGDN'] ]
terminal-onclick-url-open=true
terminal-margin-vertical=0
terminal-margin-horizo​​ntal=0">~/.termux/termux.properties
echo "foreground=#C5C8C6
background=#000000
cursor=#C5C8C6
color0=#1D1F21
color1=#CC342B
color2=#198844
color3=#FBA922
color4=#16B1FB
color5=#A36AC7
color6=#3971ED
color7=#C5C8C6
color8=#969896
color9=#CC342B
color10=#198844
color11=#FBA922
color12=#16B1FB
color13=#A36AC7
color14=#3971ED
color15=#FFFFFF
color16=#F96A38
color17=#3971ED
color18=#282A2E
color19=#373B41
color20=#B4B7B4
color21=#E0E0E0">~/.termux/colors.properties
termux-reload-settings
pkg update&&
pkg install -y root-repo x11-repo&&
pkg install -y tsu curl dialog tmux tmate perl micro ranger fastfetch unzip fish htop nethogs ncdu ripgrep fd fzf bat catimg ruby||abort "依赖安装失败"
gem install lolcat
abort_update(){ echo "
$R! $@$O";[ "$N" -lt 10 ]&&{ ((N++));download;}||abort "脚本下载失败，请检查网络，并尝试重新下载";}
download(){ case "$N" in
  2)SERVER="GitHub" URL="https://github.com/TimeRainStarSky/TRSS_OneBot/raw/main";;
  1)SERVER="Gitee" URL="https://gitee.com/TimeRainStarSky/TRSS_OneBot/raw/main";;
  3)SERVER="Agit" URL="https://agit.ai/TimeRainStarSky/TRSS_OneBot/raw/branch/main";;
  4)SERVER="Coding" URL="https://trss.coding.net/p/TRSS/d/OneBot/git/raw/main";;
  5)SERVER="GitLab" URL="https://gitlab.com/TimeRainStarSky/TRSS_OneBot/raw/main";;
  6)SERVER="GitCode" URL="https://gitcode.net/TimeRainStarSky1/TRSS_OneBot/raw/main";;
  7)Server="GitLink" URL="https://gitlink.org.cn/api/TimeRainStarSky/TRSS_OneBot/raw?ref=main&filepath=";;
  8)SERVER="JiHuLab" URL="https://jihulab.com/TimeRainStarSky/TRSS_OneBot/raw/main";;
  9)SERVER="Jsdelivr" URL="https://cdn.jsdelivr.net/gh/TimeRainStarSky/TRSS_OneBot@main";;
  10)SERVER="Bitbucket" URL="https://bitbucket.org/TimeRainStarSky/TRSS_OneBot/raw/main"
esac
echo "
  正在从 $SERVER 服务器 下载版本信息"
GETVER="$(geturl "$URL/version")"||abort_update "下载失败"
NEWVER="$(sed -n s/^version=//p<<<"$GETVER")"
NEWNAME="$(sed -n s/^name=//p<<<"$GETVER")"
NEWMD5="$(sed -n s/^md5=//p<<<"$GETVER")"
[ -n "$NEWVER" ]&&[ -n "$NEWNAME" ]&&[ -n "$NEWMD5" ]||abort_update "下载文件版本信息缺失"
echo "
$B  最新版本：$G$NEWNAME$C ($NEWVER)$O

  开始下载"
mkdir -vp "$DIR"
geturl "$URL/Main.sh">"$DIR/Main.sh"||abort_update "下载失败"
[ "$(md5sum "$DIR/Main.sh"|head -c 32)" != "$NEWMD5" ]&&abort_update "下载文件校验错误"
echo -n "exec bash '$DIR/Main.sh' "'"$@"'>"$CMDPATH/$CMD"&&chmod 755 "$CMDPATH/$CMD"||abort "脚本执行命令 $CMDPATH/$CMD 设置失败，手动执行命令：bash '$DIR/Main.sh'"
echo "
$G- 脚本安装完成，启动命令：$CMD$O";exit;}
echo "
$Y- 正在下载脚本$O"
geturl(){ curl -L --retry 2 --connect-timeout 5 "$@";}
N=1
download